MNL2 RxJS Workshop - 2023-10-04

Reactive thinking:
Define Sources (Subject as Observables or HttpClient as Example)
Define Sinks
Connect Soures and Sinks via Operators

keep never subscribe in pipe (unintended side effexct) instead flatten your pipe

Basic Operators:
tap
map
filter
delay
debounceTime
disticnctUntilChanged

Combination Operators:
startWith
combineLatest
withLatestFrom
merge

Flattening Operators:
switchMap
mergeMap
concatMap

Closing Observable:
explicit (.unsubscribe() function)
implicit (take, first, takeUntilDestroyed)
implicit async pipe (async automaticly unsubscribes)

https://rxjs.dev/
https://rxmarbles.com/
https://rxjs.dev/operator-decision-tree
