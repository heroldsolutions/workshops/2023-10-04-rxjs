import { AsyncPipe, JsonPipe } from '@angular/common';
import {
  Component,
  inject,
  OnDestroy,
  OnInit,
  DestroyRef,
} from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import {
  interval,
  combineLatest,
  combineLatestWith,
  debounceTime,
  delay,
  concatMap,
  distinctUntilChanged,
  filter,
  map,
  merge,
  mergeAll,
  mergeMap,
  mergeWith,
  of,
  startWith,
  switchMap,
  take,
  tap,
  withLatestFrom,
} from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  standalone: true,
  imports: [JsonPipe, ReactiveFormsModule, AsyncPipe, HttpClientModule],
  styles: [],
  template: `
    My Component
    <div>
      <input placeholder="source1" [formControl]="source1" />
      <input placeholder="source2" [formControl]="source2" />
    </div>
    <div>
      sink:
      {{ sink$ | async | json }}
    </div>
    <div>
      sink2 delay:
      {{ sink2$ | async | json }}
    </div>
    <div>
      sink3 debounceTime:
      {{ sink3$ | async | json }}
    </div>
    <div>
      sink4 combineLatest:
      {{ sink4$ | async | json }}
    </div>
    <div>
      sink5 withLatestFrom:
      {{ sink5$ | async | json }}
    </div>
    <div>
      sink6 mergeWith:
      {{ sink6$ | async | json }}
    </div>
    <div>
      sink7 http:
      {{ sink7$ | async | json }}
    </div>

    ----
    <div>
      switch:
      {{ sinkSwitch$ | async | json }}
    </div>
    <div>
      merge:
      {{ sinkMerge$ | async | json }}
    </div>
    <div>
      concat:
      {{ sinkConcat$ | async | json }}
    </div>
  `,
})
export default class ExampleComponent implements OnDestroy, OnInit {
  public source1 = new FormControl('');
  public source2 = new FormControl('');

  public http = inject(HttpClient);

  public sink$ = this.source1.valueChanges.pipe(
    startWith(''),
    map((value) => {
      localStorage.setItem('myItem', String(value));
      return value;
    }),
    // tap((value) => {
    //   localStorage.setItem('myItem', String(value));
    // }),
    map((value) => `${value} ${value}`),
    tap(console.log),
    map((value) => {
      // console.log(localStorage.getItem('myItem'));
      return value;
    }),
    map((value) => value.length)
    // filter((length) => length > 5)
  );

  public sink2$ = this.sink$.pipe(delay(2000));
  public sink3$ = this.sink$.pipe(
    debounceTime(2000),
    distinctUntilChanged(),
    map(() => {
      return Math.random();
    })
  );

  public sink4$ = this.source1.valueChanges.pipe(
    combineLatestWith(this.source2.valueChanges),
    tap(console.log),
    map(([source1Value, source2Value]) => `${source1Value} ${source2Value}`)
  );

  public sink5$ = this.source1.valueChanges.pipe(
    withLatestFrom(this.source2.valueChanges),
    tap(console.log),
    map(([source1Value, source2Value]) => `${source1Value} ${source2Value}`)
  );

  public sink6$ = this.source1.valueChanges.pipe(
    mergeWith(this.source2.valueChanges),
    tap(console.log)
  );

  public sink7$ = this.source1.valueChanges.pipe(
    startWith(''),
    filter((value) => String(value).length > 0),
    debounceTime(250),
    distinctUntilChanged(),
    switchMap((value) =>
      this.http.get(`https://jsonplaceholder.typicode.com/todos/${value}`)
    ),
    tap(console.log),
    switchMap((response) =>
      this.http.get(
        `https://jsonplaceholder.typicode.com/todos/${
          response.id + response.id
        }`
      )
    ),
    tap(console.log)
  );

  public simulateFirebase(val: any, delay: number) {
    return interval(delay).pipe(
      map((index) => val + ' ' + index),
      take(3)
    );
  }

  sinkSwitch$ = this.source1.valueChanges.pipe(
    switchMap((num) => this.simulateFirebase(num, 5000))
  );

  sinkMerge$ = this.source1.valueChanges.pipe(
    mergeMap((num) => this.simulateFirebase(num, 5000))
  );

  sinkConcat$ = this.source1.valueChanges.pipe(
    concatMap((num) => this.simulateFirebase(num, 5000))
  );

  subscription = this.source1.valueChanges.subscribe();

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  constructor(private readonly destroyRef: DestroyRef) {
    this.source1.valueChanges.pipe(takeUntilDestroyed()).subscribe();
  }

  public ngOnInit(): void {
    this.source1.valueChanges
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe();
  }
}
